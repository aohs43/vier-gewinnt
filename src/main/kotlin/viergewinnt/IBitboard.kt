package viergewinnt

interface IBitboard
{
    val root: IBitboard?
    val bitboard: LongArray
    val height: IntArray
    val counter: Int
    val moves: ArrayList<Int>

    fun makeMove(col: Int): IBitboard
    fun undoMove(): IBitboard
    fun isWin(player: Int): Boolean
    fun listMoves(): ArrayList<Int>
}