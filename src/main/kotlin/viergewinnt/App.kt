package viergewinnt

import io.javalin.Javalin

fun main(args: Array<String>)
{
    //setup javalin
    val app = Javalin.create().start(7070)
    app.config.addStaticFiles("/public")

    //initialize a new game
    var game = ConnectFour(null, Bitboard(null, longArrayOf(0L, 0L), intArrayOf(0, 7, 14, 21, 28, 35, 42), 0, ArrayList()))

    //handler for a new game
    app.get("/newgame") { ctx ->
        game = ConnectFour(null, Bitboard(null, longArrayOf(0L, 0L), intArrayOf(0, 7, 14, 21, 28, 35, 42), 0, ArrayList()))
        val jsonstring = createJSON(game.toString(), "Neues Spiel gestartet, du bist dran!", game.bitboard.height, "NEW GAME")
        ctx.result(jsonstring)
    }

    //handler for making a move (only human)
    app.get("/move") { ctx ->
        val col = ctx.queryParam("column")?.toIntOrNull()
        if(col != null)
        {
            game = game.playMove(col) as ConnectFour
            if(game.isWin(0) || game.isWin(1))
                ctx.result(createJSON(game.toString(), "Das Spiel ist vorbei, Du hast gewonnen!", game.bitboard.height, "GAME OVER"))
            else if(game.listMoves().isEmpty())
                ctx.result(createJSON(game.toString(), "Das Spiel ist vorbei, unentschieden!", game.bitboard.height, "GAME OVER"))
            else
                ctx.result(createJSON(game.toString(), "Du hast einen Zug in Reihe " + (col+1) + " gemacht. Der Computer wird nun seinen Zug berechnen.", game.bitboard.height, "CPU TURN"))
        }
    }

    //handler for undoing a move
    app.get("/undomove") { ctx ->
        game = game.undoMove().undoMove() as ConnectFour
        ctx.result(createJSON(game.toString(), "Du hast deinen letzten Zug zurückgenommen.", game.bitboard.height, "HUMAN TURN"))
    }

    //handler for cpu move
    app.get("/cpu") { ctx ->
        val cputime = kotlin.system.measureTimeMillis { game = game.cpuMove() as ConnectFour }
        if(game.isWin(0) || game.isWin(1))
            ctx.result(createJSON(game.toString(), "Das Spiel ist vorbei, der Computer hat gewonnen!", game.bitboard.height, "GAME OVER"))
        else if(game.listMoves().isEmpty())
            ctx.result(createJSON(game.toString(), "Das Spiel ist vorbei, unentschieden!", game.bitboard.height, "GAME OVER"))
        else
        {
            if(game.bitboard.counter and 1 == 0)
            {
                ctx.result(createJSON(game.toString(), "Der CPU hat einen Zug in Reihe "+ (game.bitboard.moves.last()+1) +" gemacht. Berechnungsdauer: " + cputime + "ms.", game.bitboard.height, "HUMAN TURN"))
            }
            else
            {
                ctx.result(createJSON(game.toString(), "Der CPU hat einen Zug für dich in Reihe "+ (game.bitboard.moves.last()+1) +" gemacht. Berechnungsdauer: " + cputime + "ms. Er wird nun seinen Zug berechnen.", game.bitboard.height, "CPU TURN"))
            }
        }
    }

    //handler for tests
    app.get("/test") { ctx ->
        val depth = ctx.queryParam("tiefe")
        game = game.test(depth!!)
        ctx.result(createJSON(game.toString(), "[TEST] Sichttiefe: "+depth+", drücke auf 'CPU Zug' um das Szenario schrittweise auszuspielen.", game.bitboard.height, "TEST MODE"))
    }
}

fun createJSON(board: String, console: String, heights: IntArray, status: String): String
{
    var s = "{"
    s += "\"board\": \""+board+"\","
    s += "\"console\": \""+console+"\","
    s += "\"heights\": ["+heights[0]+", "+(heights[1]-7)+","+(heights[2]-14)+","+(heights[3]-21)+","+(heights[4]-28)+","+(heights[5]-35)+"],"
    s += "\"status\": \""+status+"\","
    s += "\"ratings\": \""+ratingString()+"\""
    s += "}"
    return s
}

fun ratingString(): String
{
    var res = ""
    val lmr = java.io.File("lastmoverating")
    if(!lmr.exists()) return ""
    var i = 0L
    lmr.forEachLine {
        if(i and 1 == 0L) res += "Move: " + it.toInt()
        if(i and 1 == 1L) res += " ==> " + it.toInt() + "<hr>"
        i++
    }
    return res
}