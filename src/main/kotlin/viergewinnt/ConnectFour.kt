package viergewinnt

import IConnectFour

class ConnectFour(private val _root: IConnectFour?, private val _bitboard: Bitboard): IConnectFour
{
    override val root = _root
    override val bitboard = _bitboard
    companion object { val map = HashMap<Int, Int>() }

    override fun playMove(move: Int): IConnectFour
    {
        return ConnectFour(this, bitboard.makeMove(move) as Bitboard)
    }

    override fun cpuMove(): IConnectFour
    {
        loadDB()
        var bestMoves = ArrayList<Int>()
        val moveRating = ArrayList<Pair<Int, Int>>()
        if(bitboard.counter and 1 == 1)
        {
            var eval = 999999999
            for (move in listMoves()) {
                var alphabeta = alphabeta(bitboard.makeMove(move) as Bitboard, 3, -999999999, 999999999)
                moveRating.add(Pair(move, alphabeta))
                if (alphabeta <= eval) {
                    if (alphabeta < eval) bestMoves.clear()
                    bestMoves.add(move)
                    eval = alphabeta
                }
            }
        }
        else
        {
            var eval = -999999999
            for (move in listMoves())
            {
                var alphabeta = alphabeta(bitboard.makeMove(move) as Bitboard, 3, -999999999, 999999999)
                moveRating.add(Pair(move, alphabeta))
                if (alphabeta >= eval)
                {
                    if (alphabeta > eval) bestMoves.clear()
                    bestMoves.add(move)
                    eval = alphabeta
                }
            }
        }
        saveDB(moveRating)
        return this.playMove(bestMoves.random())
    }

    override fun undoMove(): IConnectFour
    {
        return root ?: this //if(root != null) return root else return this
    }

    override fun listMoves(): ArrayList<Int>
    {
        return bitboard.listMoves()
    }

    override fun isWin(player: Int): Boolean
    {
        return bitboard.isWin(player)
    }

    private fun alphabeta(bitboard: Bitboard, depth: Int, _alpha: Int, _beta: Int): Int
    {
        if(evaluate(bitboard) != 0) return evaluate(bitboard)
        val key = Pair(bitboard.bitboard, bitboard.counter and 1).hashCode()
        if(map.containsKey(key)) return map[key]!!
        if(depth <= 1)
        {
            var rating = montecarlo(bitboard)
            if(bitboard.counter-1 and 1 == 1) rating *= -1
            map[key] = rating
            return rating
        }

        var alpha = _alpha
        var beta = _beta

        if((bitboard.counter and 1) == 0) //human
        {
            var maxEval = -999999999
            for(move in listMoves())
            {
                val eval = alphabeta(bitboard.makeMove(move) as Bitboard, depth-1, alpha, beta)
                maxEval = kotlin.math.max(maxEval, eval)
                alpha = kotlin.math.max(alpha, eval)
                if(beta <= alpha) break
            }
            return maxEval
        }
        else //cpu
        {
            var minEval = 999999999
            for(move in listMoves())
            {
                val eval = alphabeta(bitboard.makeMove(move) as Bitboard, depth-1, alpha, beta)
                minEval = kotlin.math.min(minEval, eval)
                beta = kotlin.math.min(beta, eval)
                if(beta <= alpha) break
            }
            return minEval
        }
    }

    private fun montecarlo(bitboard: Bitboard): Int
    {
        var rating = 0
        for(i in 0..2000) //simulate games
        {
            var draw = false
            var bb = bitboard
            while(!bb.isWin(bb.counter-1 and 1))
            {
                if(bb.listMoves().isEmpty()) { draw = true; break }
                bb = bb.makeMove(bb.listMoves().random()) as Bitboard
            }
            if(bb.counter-1 and 1 == bitboard.counter-1 and 1 && !draw) rating++ else rating--
        }
        return rating
    }

    private fun evaluate(bitboard: Bitboard): Int
    {
        if(bitboard.isWin(0)) return 999999999 - bitboard.counter
        if(bitboard.isWin(1)) return -999999999 + bitboard.counter
        else return 0
    }

    fun test(depth: String): ConnectFour
    {
        var testgame = ConnectFour(null, Bitboard(null, longArrayOf(0L, 0L), intArrayOf(0, 7, 14, 21, 28, 35, 42), 0, ArrayList()))
        if(depth == "1")
        {
            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
            testgame = testgame.playMove(1) as ConnectFour
        }
        else if(depth == "2")
        {
            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
            testgame = testgame.playMove(0) as ConnectFour
        }
        else if(depth == "3")
        {
            //alternative
            /*testgame = testgame.playMove(2) as ConnectFour
            testgame = testgame.playMove(3) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour
            testgame = testgame.playMove(3) as ConnectFour
            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(2) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour*/

            testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(2) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour
            testgame = testgame.playMove(6) as ConnectFour
        }
        else if(depth == "4")
        {
            testgame = testgame.playMove(3) as ConnectFour
            testgame = testgame.playMove(3) as ConnectFour
            testgame = testgame.playMove(0) as ConnectFour
        }
        else if(depth == "5")
        {
            /*testgame = testgame.playMove(0) as ConnectFour
            testgame = testgame.playMove(2) as ConnectFour*/
            testgame = testgame.playMove(1) as ConnectFour
            testgame = testgame.playMove(2) as ConnectFour
            testgame = testgame.playMove(1) as ConnectFour
            testgame = testgame.playMove(1) as ConnectFour
            testgame = testgame.playMove(3) as ConnectFour
            testgame = testgame.playMove(3) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour
            testgame = testgame.playMove(5) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour
            testgame = testgame.playMove(5) as ConnectFour
            testgame = testgame.playMove(4) as ConnectFour
        }
        return testgame
    }

    private fun saveDB(mr: ArrayList<Pair<Int, Int>>)
    {
        val db = java.io.File("database")
        db.printWriter().use { out ->
            for(key in map.keys)
            {
                out.println(key.toString())
                out.println(map[key].toString())
            }
        }

        val lmr = java.io.File("lastmoverating")
        lmr.printWriter().use { out ->
           for(move in mr)
           {
               out.println(move.first.toString())
               out.println(move.second.toString())
           }
        }
    }

    private fun loadDB()
    {
        val db = java.io.File("database")
        if(!db.exists()) return
        var i = 0L
        var key = 0
        db.forEachLine {
            if(i and 1 == 0L) key = it.toInt()
            if(i and 1 == 1L) map[key] = it.toInt()
            i++
        }
    }

    override fun toString(): String
    {
        var x = ""
        var s = "<tbody>"

        for(row in 5 downTo 0) //zeilen von oben nach unten
        {
            s += "<tr>"
            for(col in 0..6) //spalten von links nach rechts
            {
                x = if ((bitboard.bitboard[0] shr (row+col*7) and 1) == 1L) "O"
                else if ((bitboard.bitboard[1] shr (row+col*7) and 1) == 1L) "X"
                else " "
                s += "<td class=\'col-1\' onclick=\'makeMove("+col+")\'> " + x + " </td>"
            }
            s += "</tr>"
        }

        s += "</tbody>"
        return s
    }
}