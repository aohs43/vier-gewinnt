package viergewinnt
/*
  6 13 20 27 34 41 48   55 62     Additional row
+---------------------+
| 5 12 19 26 33 40 47 | 54 61     top row
| 4 11 18 25 32 39 46 | 53 60
| 3 10 17 24 31 38 45 | 52 59
| 2  9 16 23 30 37 44 | 51 58
| 1  8 15 22 29 36 43 | 50 57
| 0  7 14 21 28 35 42 | 49 56 63  bottom row
+---------------------+

               0 0 0 0 0 0 0  0 0 0 0 0 0 0   6 13 20 27 34 41 48
. . . . . . .  0 0 0 0 0 0 0  0 0 0 0 0 0 0   5 12 19 26 33 40 47
. . . . . . .  0 0 0 0 0 0 0  0 0 0 0 0 0 0   4 11 18 25 32 39 46
. . . . . . .  0 0 0 0 0 0 0  0 0 0 0 0 0 0   3 10 17 24 31 38 45
. . . O . . .  0 0 0 0 0 0 0  0 0 0 1 0 0 0   2  9 16 23 30 37 44
. . . X X . .  0 0 0 1 1 0 0  0 0 0 0 0 0 0   1  8 15 22 29 36 43
. . O X O . .  0 0 0 1 0 0 0  0 0 1 0 1 0 0   0  7 14 21 28 35 42
-------------
0 1 2 3 4 5 6

... 0000000 0000000 0000010 0000011 0000000 0000000 0000000 // encoding Xs
... 0000000 0000000 0000001 0000100 0000001 0000000 0000000 // encoding Os
      col 6   col 5   col 4   col 3   col 2   col 1   col 0
*/

class Bitboard(private val _root: IBitboard?, private val _bitboard: LongArray, private val _height: IntArray, private val _counter: Int, private val _moves: ArrayList<Int>): IBitboard
{
    override val root = _root
    override val bitboard = _bitboard
    override val height = _height
    override val counter = _counter
    override val moves = _moves

    override fun makeMove(col: Int): IBitboard
    {
        //make variable copies that are passed to the new Bitboard
        var newBitboard = bitboard.copyOf()
        var newHeight = height.copyOf()
        var newCounter = counter

        //make move and do according changes
        val move: Long = 1L shl height[col]
        newHeight[col] = height[col] + 1
        newBitboard[counter and 1] = bitboard[counter and 1] xor move
        moves.clear()
        moves.add(col) //führt zu overflow bei zu großer monte carlo simulation
        newCounter = counter + 1

        //return the new Bitboard
        return Bitboard(this, newBitboard, newHeight, newCounter, moves)
    }

    override fun undoMove(): IBitboard
    {
        return root ?: this //if(root != null) return root else return this
    }

    override fun isWin(player: Int): Boolean //player: human = 0, cpu = 1 (human always starts)
    {
        val directions = intArrayOf(1, 7, 6, 8)
        var bb: Long
        for(direction in directions)
        {
            bb = bitboard[player] and (bitboard[player] shr direction)
            if ((bb and (bb shr (2 * direction))) != 0L) return true
        }
        return false
    }

    override fun listMoves(): ArrayList<Int>
    {
        val moves: ArrayList<Int> = ArrayList()
        val top = 0b1000000_1000000_1000000_1000000_1000000_1000000_1000000L
        for(col in 0..6)
            if ((top and (1L shl height[col])) == 0L) moves.add(col)
        return moves
    }
}