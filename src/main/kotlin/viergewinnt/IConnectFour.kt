import viergewinnt.Bitboard

interface IConnectFour
{
    val root: IConnectFour?
    val bitboard: Bitboard

    fun playMove(move: Int): IConnectFour
    fun cpuMove(): IConnectFour
    fun undoMove(): IConnectFour
    fun listMoves(): ArrayList<Int>
    fun isWin(player: Int): Boolean
    override fun toString(): String
}