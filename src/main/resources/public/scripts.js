var http = new XMLHttpRequest();
var game;
var heights;

function makeMove(column)
{
    if(heights[column] >= 6 || game.status == "CPU TURN" || game.status == "TESTMODE" || game.status == "GAME OVER") return
    http.open("GET", "move?column="+column);
    http.send();
}

function cpuMove()
{
    http.open("GET", "cpu");
    http.send();
}

function undoMove()
{
    if(game.status == "CPU TURN") return
    http.open("GET", "undomove");
    http.send();
}

function newGame()
{
    http.open("GET", "newgame");
    http.send();
    document.getElementById("console").innerHTML = "";
}

function test(depth)
{
    http.open("GET", "test?tiefe="+depth);
    http.send();
}

http.onreadystatechange = function()
{
    if (this.readyState === 4 && this.status === 200)
    {
        game = JSON.parse(http.responseText);

        document.getElementById("boardtable").innerHTML = game.board;

        var consoleHistory = document.getElementById("console").innerHTML;
        document.getElementById("console").innerHTML = game.console + "<hr>" + consoleHistory;

        heights = game.heights;
        document.getElementById("lmr").innerHTML = game.ratings;

        if(game.status == "CPU TURN" /*|| game.status == "TEST MODE" uncomment to execute cpu move right after loading*/)
        {
            cpuMove()
        }
    }
}