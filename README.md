# Vier Gewinnt! aka Connect Four! (PiS, SoSe 2020) 

Autor: Alex Ochs, 5272927

Ich habe die Zulassung für PiS im SoSe 2020 bei Herrn Herzberg erhalten.

<Inhaltsverzeichnis>

## Einleitung

### Spielregeln

Ihnen sollte "Vier gewinnt" sicherlich bekannt sein.
Der Spieler (O) hat immer den ersten Zug.
Abwechselnd legt man ein Zeichen in eine verfügbare Reihe.
Der erste Spieler mit 4 Zeichen in einer Reihe (horizontal, vertikal, diagonal) gewinnt.

### Bedienungsanleitung

![Screenshot](screenshot.png)

Nach dem Start mit "gradle run" ist das GUI über localhost:7070 zu erreichen.

* **Neues Spiel:** Startet ein neues Spiel.
* **CPU Zug:** Lässt den Computer Ihren Zug spielen. (Der CPU spielt immer automatisch nach Ihrem Zug.)
* **Zug zurücknehmen:** Nimmt **Ihren** Zug zurück. Es wird also das vorletzte Bitboard ausgegeben (vor dem Zug des CPUs).
* **Test ausführen:** Lädt das von Ihnen gewählte Test-Bitboard.

* **Konsole:** Hier kommuniziert das Spiel mit Ihnen und informiert Sie über die momentane Spielsituation.
* **Letzte CPU Move Ratings:** Hier werden die CPU-Move-Ratings ausgegeben, nach denen sich der Computer/Algorithmus bei seinem letzten Zug entschieden hat. Für den Computer (X) ist eine niedrige Zahl gut, für den Spieler (O) eine hohe.

### Dateiübersicht
    .
    ├── build.gradle
    ├── database
    ├── lastmoverating (wird erst nach CPU-Zug erstellt)
    ├── README.md
    ├── screenshot.png
    └── src
        └── main
            ├── kotlin
            │   └── viergewinnt
            │       ├── App.kt
            │       ├── Bitboard.kt
            │       ├── ConnectFour.kt
            │       ├── IBitboard.kt
            │       └── IConnectFour.kt
            └── resources
                └── public
                    ├── index.html
                    ├── scripts.js
                    └── styles.css

    -------------------------------------------------------------------------------
    Language                     files          blank        comment           code
    -------------------------------------------------------------------------------
    Kotlin                           5             43             43            370
    Markdown                         1             44              0            140
    HTML                             1              4              4            107
    JavaScript                       1             10              0             47
    CSS                              1              4              0             18
    Gradle                           1              9             14             18
    -------------------------------------------------------------------------------
    SUM:                            10            114             61            700
    -------------------------------------------------------------------------------
    Ohne Md/Gradle:                                                             542
    JavaScript-Anteil:                                              140/542 = 8.67%  
    -------------------------------------------------------------------------------

## Spiel-Engine (ENG)

Feature    | AB  | H   | MC  | eD | B+I+Im  | Summe
-----------|-----|-----|-----|-----|------|----
Umsetzung  | 120 | 70  | 100 | 130 | 99.9 |
Gewichtung | 0.4 | 0.3 | 0.3 | 0.3 |  0.3 | 
Ergebnis   |  ?  |  ?  |  ?  |   ? |   ?  | **128%**

Der Kern der Engine ist die *ConnectFour*-Klasse, sie repräsentiert eine Instanz des Spiels und ist vollkommen immutabel.
Neben Funktionen besitzt die Klasse eine "Root-Instanz" (um Züge zurückzunehmen), ein **Bitboard** und eine **Hash-Map**, welche sich alle Klassen-Instanzen teilen.

Die Interaktion zwischen GUI und Engine geschieht in *App.kt* bzw. *Javalin*.
Je nach Request erstelle ich eine neue *ConnectFour*-Instanz ausgehend von der Momentanen.
Alle notwendigen Daten der neuen Spiel-Instanz werden dann in einen JSON-String verpackt und an das GUI bzw. den Browser zurückgegeben.

**Alpha-Beta:** Der Alpha-Beta-Algorithmus ist in der *alphabeta*-Funktion von *ConnectFour* zu finden.
Als Basis habe ich [dieses Video](https://www.youtube.com/watch?v=l-hh51ncgDI) verwendet. 
Sie wird stets mit einem bereits gespieltem Bitboard als Parameter aufgerufen.
Zuerst wird überprüft ob der bereits gemachte Zug zu einem Gewinn geführt hat, ist dies der Fall so gibt *alphabeta* den entsprechenden Wert zurück, andernfalls verläuft *alphabeta* wie ein gewohnter Alpha-Beta-Algorithmus ab.
Je nachdem wer nun am Zug ist wird *alphabeta* rekursiv für alle möglichen Züge aufgerufen und der bestmögliche Zug zurückgegeben.
Möchte man nun einen Zug dem Computer überlassen, so wird allerdings *cpuMove* aufgerufen und nicht *alphabeta*.
*cpuMove* ruft *alphabeta* für alle möglichen Züge auf und wählt daraus den besten Zug, allerdings geschehen auch noch einige Kleinigkeiten welche nicht in *alphabeta* inbegriffen sind wie das Laden/Speichern der DB und das Speichern der "Move-Ratings".

**Hash-Map:** Die Hash-Map wird als *companion object* in der *ConnectFour*-Klasse weitergegeben, d.h. alle *ConnectFour*-Instanzen teilen sich dieselbe Hash-Map.
Als Key nimmt sie das Ergebnis der *hashCode*-Funktion des Paares (Bitboard, Spieler am Zug).
Der dazugehörige Wert (Value) ist ein Rating für die Spielsituation.
Das Rating wird folgend in der Monte-Carlo-Erläuterung näher gebracht.

**Monte-Carlo:** Um das Rating für eine Spielsituation zu berechnen verwende ich Monte-Carlo-Simulationen.
Doch vorerst: Wie o.g. wird beim Aufruf von *alphabeta* erst überprüft ob das Spiel schon vorbei ist (also jemand gewonnen hat).
Ist dies der Fall so wird ±∞ - ±Counter bzw. Anzahl der Züge zurückgeben (das Endergebnis ist also abhängig von der Menge der Züge und dem Spieler der Gewonnen hat).
Erst wenn eine gegebene Alpha-Beta-Tiefe erreicht ist und es bis dahin keinen Gewinn gab, kommt die Monte-Carlo-Simulation ins Spiel.
Diese ist in der *montecarlo*-Funktion der *ConnectFour*-Klasse zu finden.
Von der momentanen Spielstellung aus wird eine gegebene Anzahl an Spielverläufen (mit zufälligen Zügen) simuliert.
Hat eine Simulation ein Ende erreicht (Gewinn oder keine möglichen Züge mehr), so wird das Rating bei einem Gewinn erhöht, Verlust erniedrigt und Gleichstand gleich gelassen.
Somit wird also eine Spielstellung mit z.B. 6 Siegen und 1 Niederlage besser gewertet als 10 Siege und 6 Niederlagen (6-1 > 10-6).

**Eigene Datenbank:** Die Datenbank ist eine Text-Datei und befindet sich im Root-Ordner des Projekts.
In Ihr wird die Hash-Map gespeichert.
Eine Zeile (die Erste) hat den Wert des Keys, in der folgenden befindet sich die dazugehörige Value.
Bei einem Aufruf der *cpuMove*-Funktion wird die Datenbank Zeile für Zeile eingelesen und in die Hash-Map übertragen (siehe *loadDB*-Funktion).
Nachdem der Computer seinen Zug berechnet hat wird die Hash-Map wieder in die Datenbank geschrieben (siehe *saveDB*-Funktion).
Bei Bedarf kann also die Datenbank gelöscht, Alpha-Beta-Tiefe und Monte-Carlo-Simulation neu bestimmt und die Datenbank wieder neu aufgebaut werden.
Zum Lese-/Schreibevorgang der Text-Datei:
Leider muss man hier die *File*-Klasse der *java.io*-Bibliothek nutzen, da Kotlin für solche Vorgänge keine eigenen Lösungen/Bibliotheken bietet bzw. die von Java nutzt.

**Bitboard:** Bei der Umsetzung des Bitboards habe ich mich **stark** an [D. Herzbergs Design und Pseudocode](https://github.com/denkspuren/BitboardC4/blob/master/BitboardDesign.md) gehalten.
Da die Bitboards sogar schon für "Vier gewinnt" konstruiert waren, musste ich also nicht viel an der Logik ändern, sondern hauptsächlich das Design in Kotlin übertragen.
Als Repräsentation werden zwei Longs benutzt, jeweils pro Spieler, und durch die *makeMove*-Funktion verändert.
*undoMove* gibt das vorherige Bitboard zurück, *isWin* überprüft ob der gegebene Spieler gewonnen hat und *listMoves* gibt eine Liste mit allen möglichen Reihen/Moves zurück.

**Interface:** Jeweils beide Klassen (*ConnectFour* und *Bitboard*) haben ihr dazugehöriges Interface (*IConnectFour* und *IBitboard*).
*Bitboard* ist eine 1:1 Übertragung des *IBitboard*-Interfaces, ohne extra Funktionen oder Daten.
*ConnectFour* hingegen besitzt einige (private) Funktionen die nicht im *IConnectFour*-Interface inbegriffen sind (z.B. *load-/saveDB*, *alphabeta*).
Das *ConnectFour*-Interface bietet dennoch eine stabile Basis falls man weitere *ConnectFour*-Varianten implementieren möchte.

**Immutabilität:** Beide Klassen (*ConnectFour* und *Bitboard*) sind komplett immutabel implementiert.
Alle Klassen-Variablen sind mit *val* initialisiert.
Alle Funktionen, welche die Spielsituation ändern (also alle *move*-Funktion), liefern eine neue Klassen-Instanz.
Alle elementaren Daten (außer die Hash-Map) einer Instanz sind somit alle statisch und immutabel.

## Tests (TST)

Szenario |  1  |  2  |  3  |  4  |  5  | Summe
---------|-----|-----|-----|-----|-----|-------
ok       |  X  |  X  |  -  |  X  |  X  | 0.8

Die Tests werden wie folgt ausgeführt:
Drücken Sie auf den Button "Test ausführen" und wählen Sie ein eine Sichttiefe (Sichttiefe 1 ≠ Szenario 1).
1. Die Spiel-Engine kann im nächsten Zug gewinnen (Sichttiefe 1)
2. Die Spiel-Engine kann im übernächsten Zug gewinnen (Sichttiefe 3)
3. Die Spiel-Engine kann im überübernächsten Zug gewinnen (Sichttiefe 5)
4. Die Spiel-Engine vereitelt eine unmittelbare Gewinnbedrohung des Gegners (Sichttiefe 2)
5. Die Spiel-Engine vereitelt ein Drohung, die den Gegner im übernächsten Zug ansonsten einen Gewinn umsetzen lässt (Sichttiefe 4)

Es wird nun das gewählte Szenario erstellt (siehe *test*-Funktion in *ConnectFour*).
Folgend wird über den "CPU Zug"-Button der CPU zum Zug aufgefordert.

Die Testausführung/-wertung findet weniger in der Konsole und mehr im GUI statt.
Die Auswertung erfolgt durch Beobachtung des Spielbretts und den Move-Ratings.
Beispielsweise würde bei Sichttiefe 1 nur ein Zug ganz rechts den Test erfüllen (Gewinn mit nächstem Zug), bei Sichttiefe 2 ein Zug ganz links (Gewinn des Gegners vereiteln).

Die Testausführung protokolliert sich über die Konsole wie folgt (gelesen von unten nach oben):

    Der CPU hat einen Zug in Reihe 1 gemacht. Berechnungsdauer: 367ms.

    [TEST] Sichttiefe: 2, drücke auf 'CPU Zug' um das Szenario schrittweise auszuspielen.

    Das Spiel ist vorbei, der Computer hat gewonnen!

    [TEST] Sichttiefe: 1, drücke auf 'CPU Zug' um das Szenario schrittweise auszuspielen.

## Umsetzung der GUI
Für die Oberfläche benutze ich Bootstrap 5.
Die Website ist aufgeteilt in *index.html*, *scripts.js* und *styles.css*.
Ich wollte ein einfaches und überschaubares Design.
Im Fokus steht die Mitte mit dem Spielbrett und den Buttons zur rechten Seite.
Darunter befindet sich die Konsole welche zur Kommunikation und Einsicht dient, und die Move-Ratings des letzten CPU-Zugs um die Denkweise des Computers transparenter zu gestalten.

Um das Spielbrett umzusetzen benutze ich eine Tabelle, wobei ein Klick auf eine Reihe die *makeMove*-Funktion mit der jeweiligen Reihe ausführt.
Die Buttons lösen alle eine JavaScript-Funktion aus mit der jeweiligen Request an den Server.
Die Response vom Server ist immer ein JSON-String mit verschiedenen Daten der *ConnectFour*-Instanz (generiert von der *createJSON*-Funktion in *App.kt*).

Dazu gehören:
* Spielbrett/Bitboard als HTML-Tabelle
* Output für die Konsole
* Die Höhen der Reihen
* Ein Status des Spiels (z.B. HUMAN TURN, GAME OVER, etc.)
* Move-Ratings des letzten CPU-Zugs

Wenn das GUI/der Browser seine Response erhalten hat, wird der JSON-String geparsed und alle Elemente der Website aktualisiert.
So entsteht eine einfache und transparente UX durch das GUI.

## Hinweise

Falls der CPU zu lange braucht um einen Zug zu berechnen (was eigentlich nicht der Fall sein sollte, falls eine Datenbank und realistische Werte benutzt werden), können Alpha-Beta-Tiefe und die Anzahl der Simulationen in Monte-Carlo geändert werden.
Andersrum kann bei schnellen Zügen die Genauigkeit erhöht werden indem man die Werte erhöht.

* Tiefe in der *cpuMove*-Funktion von *ConnectFour* für jeweils beide Spieler ändern (Standard: 3)
* Anzahl der Simulationen in der *montecarlo*-Funktion von *ConnectFour* ändern (Standard: 2000)

Die mitgegebene Datenbank wurde zum Teil mit Tiefe 5 und 3000 Simulationen erstellt.

Falls der CPU aus irgendeinem Grund sich öfters zu unerwarteten Zügen entscheidet oder die Ratings fehlerhaft scheinen lohnt sich ein Wiederaufbau der Datenbank oder das Justieren der o.g. Variablen. (Natürlich sollte dies niemals passieren, falls jedoch einige Werte der Datenbank fehlerhaft sind/werden oder Sonstiges, möchte ich eine mögliche Lösung anbieten).
Die Test-Szenarien z.B. wurden alle öfters hintereinander richtig gespielt, jedoch klappt dies nicht immer da z.B. durch die Monte-Carlo-Simulation ein ungenauer Wert gespeichert wird und dieser Fehler sich dann in der Datenbank verbreitet (Eine mögliche Vermutung).

Zur Sichttiefe 5 habe ich leider keine Spielsituation gefunden.

## Quellennachweis

* https://github.com/denkspuren/BitboardC4/blob/master/BitboardDesign.md
* https://www.youtube.com/watch?v=l-hh51ncgDI
* https://en.wikipedia.org/wiki/Monte_Carlo_tree_search
* https://www.youtube.com/watch?v=Fbs4lnGLS8M
* https://v5.getbootstrap.com/
* https://www.w3schools.com/
